// import React from 'react'
// import ReactDOM from 'react-dom/client'
//
// ReactDOM.createRoot(document.getElementById('root')!).render(
//   <React.StrictMode>
//   </React.StrictMode>,
// )

import {captureCamera} from "./utils.ts";
import {CanvasRenderer} from "./CanvasRenderer.ts";
import {PoseDetectionContainer} from "./PoseDetectionContainer.ts";
import {GameManager} from "./GameManager.ts";

const video: HTMLVideoElement = document.getElementById("video") as HTMLVideoElement;
const canvas: HTMLCanvasElement = document.getElementById("canvas") as HTMLCanvasElement;
const cookie: HTMLImageElement = document.getElementById("cookie") as HTMLImageElement;
const mouth: HTMLImageElement = document.getElementById("mouth") as HTMLImageElement;

const container = new PoseDetectionContainer(video);
container.startDetection();

captureCamera().then(stream => {
  const renderer = new CanvasRenderer(stream, video, canvas);
  renderer.renderCanvas();
  const manager = new GameManager(renderer, container, 0, [mouth, cookie]);
  manager.startGame();
})
