import {Pose} from "@tensorflow-models/pose-detection";
import {CanvasRenderer} from "./CanvasRenderer.ts";
import {PoseDetectionContainer} from "./PoseDetectionContainer.ts";

type Target = {
  x: number,
  y: number,
  speed: number,
  score: number,
  id: string,
}

export class GameManager {
  renderer: CanvasRenderer;
  detectionContainer: PoseDetectionContainer;
  sprites: HTMLImageElement[];
  selectedKeypoint: number;
  score: number;
  spawnInterval: number;
  baseSpeed: number;
  currentPoses: Pose[] = [];
  previousPoses: Pose[] = [];
  targets: Target[] = [];

  constructor(renderer: CanvasRenderer, detectionContainer: PoseDetectionContainer, selectedKeypoint: number, sprites: HTMLImageElement[]) {
    this.renderer = renderer;
    this.renderer.registerRenderFunction(this.moveTargets.bind(this));
    this.renderer.registerRenderFunction(this.calculateHits.bind(this));
    this.detectionContainer = detectionContainer;
    this.detectionContainer.setCallback(this.updatePoses.bind(this));
    this.sprites = sprites;
    this.selectedKeypoint = selectedKeypoint;
    this.score = 0;
    this.spawnInterval = 0;
    this.baseSpeed = 80;
  }

  startGame(){
    this.score = 0;
    const width = this.renderer.getResolution()[0];
    const offset = width / 8;
    this.spawnInterval = setInterval(() => {
      const position = Math.random() * (width - offset * 2);
      const speed = this.baseSpeed + this.score;
      this.targets.push({
        x: offset + position,
        y: 0,
        speed: speed,
        score: speed / 3,
        id: new Date().toString()
      })
    }, 1000);
  }

  stopGame() {
    clearInterval(this.spawnInterval);
    this.targets = [];
  }

  moveTargets(passedTimeInMilliseconds: number){
    for (let i = 0; i < this.targets.length; i++) {
      const target = this.targets[i];
      if(!this.renderer.hasSprite(target.id)){
        this.renderer.addSprite(
          {
            image: this.sprites[1],
            x: target.x,
            y: target.y,
            width: 30,
            height: 30,
          },
          target.id,
        )
      } else {
        target.y += target.speed * passedTimeInMilliseconds / 1000; // CSS Pixels per second
        this.renderer.moveSprite(target.id, target.x, target.y);
      }
      if(target.y > this.renderer.getResolution()[1]){
        this.renderer.deleteSprite(target.id);
        this.updateScore(-target.score);
        this.targets.splice(i, 1);
      }
    }
  }

  calculateHits(){
    let collisionIDs: string[] = [];
    for (let i = 0; i < this.currentPoses.length; i++){
      collisionIDs = collisionIDs.concat(this.renderer.getOverlappingSprites(this.getIdForKeypoint(i, this.selectedKeypoint)));
    }
    //delete each target with the ids matching the ones in collisions
    for (const collisionID of collisionIDs) {
      this.targets = this.targets.filter(target => {
        const match = target.id === collisionID;
        if(match){
          this.updateScore(target.score);
          this.renderer.deleteSprite(target.id);
        }
        return !match;
      });
    }
  }

  updatePoses(poses: Pose[]){
    this.previousPoses = this.currentPoses;
    this.currentPoses = poses;
    this.updateSpritesForSelectedKeypoint();
  }

  updateScore(delta: number) {
    if(this.score + delta > 0){
      this.score += delta;
    } else {
      this.score = 0;
    }
  }

  updateSpritesForSelectedKeypoint(){
    //Ensure the sprites match the detected keypoints
    if((this.currentPoses.length > this.previousPoses.length)){
      for (let i = this.previousPoses.length; i < this.currentPoses.length; i++) {
        const identifier = this.getIdForKeypoint(i, this.selectedKeypoint);
        this.renderer.addSprite({image: this.sprites[0], x: 0, y: 0, width: 150, height: 50}, identifier);
      }
    } else if(this.currentPoses.length < this.previousPoses.length){
      for (let i = this.currentPoses.length; i < this.previousPoses.length; i++) {
        const identifier = this.getIdForKeypoint(i, this.selectedKeypoint);
        this.renderer.deleteSprite(identifier);
      }
    }
    //Move sprites to the location of the keypoint
    if(this.currentPoses.length > 0){
      for (let i = 0; i < this.currentPoses.length; i++) {
        const selectedKeypoint = this.currentPoses[i].keypoints[this.selectedKeypoint];
        this.renderer.moveSprite(
          this.getIdForKeypoint(i, this.selectedKeypoint),
          selectedKeypoint.x - 75,
          selectedKeypoint.y +50
        );
      }
    }
  }

  getIdForKeypoint(poseIndex: number, keypointIndex: number): string {
    return `pose${poseIndex}point${keypointIndex}`;
  }

}
