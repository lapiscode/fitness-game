import {rectanglesIntersect, spriteToRect} from "./utils.ts";

export type Sprite = {
  image: CanvasImageSource | undefined,
  x: number,
  y: number,
  width: number,
  height: number,
}

export class CanvasRenderer {
  private videoElement: HTMLVideoElement;
  private canvasElement: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;
  private spritesMap: Map<string, Sprite>;
  private registeredFunctions: Array<(passedTime: number) => void>;
  private lastRender: number;

  constructor(stream: MediaStream, video: HTMLVideoElement, canvas: HTMLCanvasElement) {
    this.canvasElement = canvas;
    this.context = canvas.getContext("2d") as CanvasRenderingContext2D;
    this.spritesMap = new Map<string, Sprite>;
    this.registeredFunctions = [];
    this.registerRenderFunction(this.drawSprites.bind(this));
    this.lastRender = Date.now();
    this.videoElement = video;
    this.videoElement.srcObject = stream;
    const settings = stream.getVideoTracks()[0].getSettings();
    this.setResolution(settings.width || 1920, settings.height || 1080);
    this.videoElement.play();
  }

  registerRenderFunction(fn: (passedTime: number) => void) {
    this.registeredFunctions.push(fn);
  }

  renderCanvas(){
    this.context.setTransform(-1,0,0,1, this.canvasElement.width, 0);
    this.context.drawImage(this.videoElement, 0, 0, this.videoElement.width, this.videoElement.height);
    const now = Date.now();
    const timeDelta = now - this.lastRender;
    for (const registeredFunction of this.registeredFunctions) {
      registeredFunction(timeDelta);
    }
    this.lastRender = now;
    requestAnimationFrame(this.renderCanvas.bind(this));
  }

  drawSprites() {
    for (const [, sprite] of this.spritesMap){
      if(sprite.image){
        this.context.drawImage(sprite.image, sprite.x, sprite.y, sprite.width, sprite.height);
      } else {
        this.context.fillRect(sprite.x, sprite.y, sprite.width, sprite.height);
      }
    }
  }

  addSprite(sprite: Sprite, id?: string): string {
    id = id ? id : new Date().toString();
    this.spritesMap.set(id, sprite);
    return id;
  }

  moveSprite(id: string, x: number, y: number){
    if(this.spritesMap.has(id)){
      const entry = this.spritesMap.get(id) as Sprite;
      this.spritesMap.set(id, {...entry, x: x, y: y})
    } else {
      throw Error("Tried moving a sprite that doesn't exist");
    }
  }

  deleteSprite(id: string){
    if(this.spritesMap.has(id)){
      this.spritesMap.delete(id);
    } else {
      throw Error("Tried deleting a sprite that doesn't exist");
    }
  }

  getOverlappingSprites(id: string): string[] {
    if (this.spritesMap.has(id)) {
      const overlapping: string[] = [];
      const referenceRect = spriteToRect(this.spritesMap.get(id) as Sprite)
      for (const [id, sprite] of this.spritesMap) {
        const rect = spriteToRect(sprite);
        if (rectanglesIntersect(referenceRect, rect)){
          overlapping.push(id);
        }
      }
      return overlapping;
    } else {
      throw Error("Sprite doesn't exist")
    }
  }

  hasSprite(id:string): boolean {
    return this.spritesMap.has(id);
  }

  setResolution(width: number, height: number) {
    if (this.videoElement && this.canvasElement) {
      this.videoElement.width = width;
      this.videoElement.height = height;
      this.canvasElement.width = width;
      this.canvasElement.height = height;
    }
  }

  getResolution(): number[] {
    return [this.canvasElement.width, this.canvasElement.height];
  }
}
