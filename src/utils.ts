import {Sprite} from "./CanvasRenderer.ts";

export async function captureCamera(): Promise<MediaStream> {
  const constraints: MediaStreamConstraints = {
    audio: false,
    video: {
      width: {ideal: 4096},
      height: {ideal: 2160}
    }
  }
  return await navigator.mediaDevices.getUserMedia(constraints);
}

type Rectangle = {
  xMin: number,
  xMax: number,
  yMin: number,
  yMax: number,
}

export function rectanglesIntersect(rectA: Rectangle, rectB: Rectangle): boolean {
  const aLeftOfB = rectA.xMax < rectB.xMin;
  const aRightOfB = rectA.xMin > rectB.xMax;
  const aAboveB = rectA.yMin > rectB.yMax;
  const aBelowB = rectA.yMax < rectB.yMin;
  return !(aLeftOfB || aRightOfB || aAboveB || aBelowB);
}

export function spriteToRect(sprite: Sprite): Rectangle {
  return {
    xMin: sprite.x,
    xMax: sprite.x + sprite.width,
    yMin: sprite.y,
    yMax: sprite.y + sprite.height,
  }
}
