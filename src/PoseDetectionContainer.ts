import {
  createDetector,
  movenet,
  MoveNetModelConfig, Pose,
  PoseDetector,
  SupportedModels
} from "@tensorflow-models/pose-detection";
import '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-backend-webgl';

type PosesCallback = (poses: Pose[]) => void;

export class PoseDetectionContainer {
  private videoElement: HTMLVideoElement;
  private detectorPromise: Promise<PoseDetector>;
  private newPosesCallback: PosesCallback;

  constructor(video: HTMLVideoElement, callback?: PosesCallback) {
    this.videoElement = video;
    this.newPosesCallback = callback
      ? callback
      : () => console.warn("Pose detected but callback missing");
    const detectionModel = SupportedModels.MoveNet;
    const moveNetMultiConfig: MoveNetModelConfig = {
      modelType: movenet.modelType.MULTIPOSE_LIGHTNING
    };
    console.log("creating detector");
    this.detectorPromise = createDetector(detectionModel, moveNetMultiConfig);
  }
  async startDetection() {
    const detector = await this.detectorPromise;
    console.log("detector functional");
    setInterval(async () => {
      this.newPosesCallback(await detector.estimatePoses(this.videoElement));
    }, 50);
  }

  setCallback(callback: PosesCallback){
    this.newPosesCallback = callback;
  }
}
